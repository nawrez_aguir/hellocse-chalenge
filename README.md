Test Front-End VueJs3
Ce projet est une application Laravel Vuejs qui permettant de créer/modifier/supprimer les fiches « star » et d’afficher de manière responsive sur mobile et desktop le contenu des fiches.

💻 Installation

Pour installer l'application, suivez ces étapes :

Clonez le projet de GitHub sur votre ordinateur local.

Accédez au répertoire du projet et exécutez composer install et npm install pour installer les dépendances requises.

Créez une nouvelle base de données dans MySQL pour l'application.

Créez une copie du fichier .env.example et renommez-le en .env. Mettez à jour les informations d'identification de la base de données dans ce fichier pour qu'elles correspondent à votre base de données MySQL.

Exécutez php artisan key:generate pour générer une nouvelle clé de chiffrement pour l'application.

Exécutez php artisan migrate pour exécuter les migrations de la base de données et créer les tables nécessaires.

Exécutez php artisan storage:link pour créer un lien symbolique du répertoire public/storage vers le répertoire storage/app/public.

Démarrez le serveur de développement en exécutant php artisan serve et npm run dev en même temps

🔬 Documentation

API :
GET /api/fetch
Cet api renvoie une liste de tous les stars dans la base de données. Chaque star est représenté sous forme d'un objet JSON avec les propriétés suivantes :
id : L'identifiant unique de star dans la base de données.
first_name : le nom de star
last_name : le prénom de star
description : la description de star
img_url : L'URL de l'image de star

POST /api/create Cette api permet de créer une nouvelle fiche "star"

POST /api/update/id Cette api permet de modifier une nouvelle fiche "star"

POST /api/delete/id Cette api permet de supprimer une nouvelle fiche "star"

Backoffice :
Le backoffice vous permet de créer, modifier et supprimer des fiches "star". Pour accéder au backoffice allez à /admin

Dans le backoffice, vous pouvez voir la liste de toutes les fiches "star" existantes. Vous pouvez créer une nouvelle fiche en cliquant sur le bouton "Ajouter un star" en haut à droite de la page. Pour modifier ou supprimer une fiche existante, cliquez sur le bouton correspondant.

Pour créer ou modifier une fiche, vous devez remplir le formulaire avec le nom, le prénom, la description et l'image de la fiche.

Page publique :
La page publique affiche toutes les fiches "star" existantes. Elle est responsive et s'adapte à la taille de l'écran, que ce soit sur mobile ou desktop.

Chaque fiche est affichée avec son nom, son prénom, sa description et son image. Si vous cliquez sur un nom dans la liste, vous accédez à une partie détaillée qui affiche toutes les informations de la fiche.

🎉Conclusion
C'est ça! Vous devriez maintenant avoir une application VueJS Laravel qui peut créer, mettre à jour, supprimer et afficher une fiche star . 😄

💬Contact
Si vous avez des questions ou des commentaires sur ce projet, n'hésitez pas à me contacter à nawrezaguir@gmail.com. J'aimerais recevoir de vos nouvelles !
