<?php

namespace App\Services;

use App\Models\Stars;
use App\Services\ImageService;
class StarsService
{
    public function create($request)
    {

        $img =  (new ImageService)->store($request->file('img'));
        $star = Stars::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'description' => $request->description,
            'img_url'=>$img['path']
        ]);
        return $star;
    }

    public function update(Stars $star,$request)
    {
        $star->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'description' => $request->description,
        ]);
        if($request->img){
            (new ImageService)->destroy($star->img_url);
            $file =  (new ImageService)->store($request->file('img'));
            $star->update(['img' => $file['path']]);
        }
        return $star;
    }

    public function delete(Stars $star){
        (new ImageService)->destroy($star->img_url);
        $star->delete();

    }
}
