<?php

namespace App\Http\Controllers;

use App\Models\Stars;
use Inertia\Inertia;

class starsController extends Controller
{
    public function index()
    {
        return Inertia::render("main");
    }
}
