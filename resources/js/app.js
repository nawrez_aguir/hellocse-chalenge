import "./bootstrap";
import "../css/app.css";
// ElementPlus
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
//axios
import axios from "axios";
import VueAxios from "vue-axios";
import store from "./store";

import { createApp, h } from "vue";
import { createInertiaApp } from "@inertiajs/inertia-vue3";
import { resolvePageComponent } from "laravel-vite-plugin/inertia-helpers";
createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) =>
        resolvePageComponent(
            `./views/${name}.vue`,
            import.meta.glob("./views/**/*.vue")
        ),
    setup({ el, app, props, plugin }) {
        return createApp({ render: () => h(app, props) })
            .use(plugin)
            .use(store)
            .use(ElementPlus)
            .use(VueAxios, axios)
            .mount(el);
    },
});
