import { createStore } from "vuex";
import axios from "axios";
import star from "../model/star";
export default createStore({
    state: {
        starsData: [],
        starsDataLoading: false,
        DeletestarsLoading: false,
        newStarLoading: false,
        editStarLoading: false,
        deleteStarLoading: false,
    },
    getters: {
        getStarsData: (state) => state.starsData,
        getStarsDataLoading: (state) => state.starsDataLoading,
        getNewStarLoading: (state) => state.newStarLoading,
        getEditStarLoading: (state) => state.editStarLoading,
        getDeleteStarLoading: (state) => state.deleteStarLoading,
    },
    mutations: {
        SET_STARS_DATA(state, payload) {
            state.starsData = payload.map((i) => star.create(i));
        },
        SET_STARS_DATA_LOADING(state, payload) {
            state.starsDataLoading = payload;
        },
        ADD_NEW_STAR(state, payload) {
            state.starsData.push(star.create(payload));
        },
        EDIT_STAR(state, payload) {
            payload.star.update(payload.response);
        },
        DELETE_STAR(state, payload) {
            state.starsData = state.starsData.filter((i) => i.id != payload);
        },
        SET_NEW_STAR_LOADING(state, payload) {
            state.newStarLoading = payload;
        },
        SET_EDIT_STAR_LOADING(state, payload) {
            state.editStarLoading = payload;
        },
        SET_DELETE_STAR_LOADING(state, payload) {
            state.deleteStarLoading = payload;
        },
    },
    actions: {
        // fetch all star method
        fetchStarsData({ commit }) {
            commit("SET_STARS_DATA_LOADING", true);
            axios.get("api/fetch").then((response) => {
                commit("SET_STARS_DATA", response.data.data);
                commit("SET_STARS_DATA_LOADING", false);
            });
        },
        // add new star method
        async addNewStar({ commit }, payload) {
            commit("SET_NEW_STAR_LOADING", true);
            const response = await axios.post("api/create", payload, {
                headers: {
                    "Content-Type": "multipart/form-data",
                },
            });
            if (response) {
                commit("ADD_NEW_STAR", response.data.data);
                commit("SET_NEW_STAR_LOADING", false);
                return true;
            }
        },
        // Edit exist star method
        async EditStar({ commit }, payload) {
            commit("SET_EDIT_STAR_LOADING", true);
            const response = await axios.post(
                "api/update/" + payload.star.id,
                payload.data,
                {
                    headers: {
                        "Content-Type": "multipart/form-data",
                    },
                }
            );
            if (response) {
                commit("EDIT_STAR", {
                    response: response.data.data,
                    star: payload.star,
                });
                commit("SET_EDIT_STAR_LOADING", false);
                return true;
            }
        },
        //delete star method
        async DeleteStar({ commit }, payload) {
            commit("SET_DELETE_STAR_LOADING", true);
            const response = await axios.post("api/delete/" + payload.id);
            if (response) {
                commit("DELETE_STAR", payload.id);
                commit("SET_DELETE_STAR_LOADING", false);
                return true;
            }
        },
    },
});
