export default class star {
    constructor(data) {
        this.id = data.id;
        this.last_name = data.last_name;
        this.first_name = data.first_name;
        this.description = data.description;
        this.img_url = data.img_url;
        this.show = false;
    }
    update(data) {
        this.last_name = data.last_name;
        this.first_name = data.first_name;
        this.description = data.description;
        this.img_url = data.img_url;
    }
    static create(data) {
        return new star(data);
    }
}
